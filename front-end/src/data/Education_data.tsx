import utaustin_img from '../assets/education_images/University_of_Texas_at_Austin_seal.svg.png';
import uh_red from '../assets/education_images/uh_red.png';
import southern_methodist from '../assets/education_images/southern_methodist_university.png';

export const education_data = [
  {
    name: 'University of Texas at Austin',
    img: utaustin_img,
    description:
      "The University of Texas at Austin (UT Austin) is a public research university in Austin, Texas. Founded in 1883, the university offers more than 100 undergraduate and 170 graduate degrees, including nine honors programs. According to U.S. News & World Report's 2024 ranking, The University of Texas at Austin was ranked 32nd among all universities in the U.S. and 9th among public universities.",
    city: 'Austin',
    address: '100 W. Dean Keeton St., Austin, TX 78712',
    resource_type: 'Education',
    assistance_type: 'Scholarship',
    application_required: 'Yes',
    email: 'UTVeterans@austin.utexas.edu',
    phone: '512-232-2835',
    size: '42,444',
    statistics: '1,222',
    website: 'https://www.utexas.edu/',
    yellowRibbon: 'Yes',
    src: 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d182592.62814619823!2d-97.8018698379686!3d30.21719911583684!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8644b59b2584cfb7%3A0x8131ee4f174a21de!2sThe%20University%20of%20Texas%20at%20Austin!5e0!3m2!1sen!2sus!4v1707888238665!5m2!1sen!2sus',
    house: '/housing/',
    mental: '/healthcare/',
    video: 'https://www.youtube.com/embed/gFOZAO3WoQQ?si=Gxjf0lj7mMyXoy7f'
  },
  {
    name: 'University of Houston',
    img: uh_red,
    description:
      'The University of Houston (UofH) is a public research university in Houston, Texas. Established in 1927, the university is the third largest in Texas, offering around 310 degree programs. The university offers eligible veterans, spouses, and their dependent children up to 150 hours of tuition exemption, along with GI Bill education benefits.',
    city: 'Houston',
    address: '4300 Martin Luther King Blvd, Houston, TX 77204',
    resource_type: 'Education',
    assistance_type: 'Scholarship',
    application_required: 'Yes',
    email: 'vets@uh.edu',
    phone: '713-743-5490',
    size: '37,943',
    statistics: '1,416',
    website: 'https://www.uh.edu/',
    yellowRibbon: 'No',
    src: 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3464.995832463158!2d-95.34481932467978!3d29.719879975088656!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8640be5a8ebea199%3A0xc9f387eca8247e7!2sUniversity%20of%20Houston!5e0!3m2!1sen!2sus!4v1707889731564!5m2!1sen!2sus',
    house: '/housing/',
    mental: '/healthcare/',
    video: 'https://www.youtube.com/embed/HIgat6hcGQk?si=LCOQUcubhLiyfMh4'
  },
  {
    name: 'Southern Methodist University',
    img: southern_methodist,
    description:
      "Established in 1911, Southern Methodist University (SMU) is a private research institution located in Dallas, Texas. Offering a comprehensive range of academic programs, SMU grants 3,827 degrees, comprising 315 doctorates, 1,659 master's, and 1,853 bachelor's degrees. Additionally, the university boasts over 32 doctoral and more than 120 master's programs. As part of its commitment to supporting veterans, SMU participates in the Yellow Ribbon Program and provides a 100% tuition benefit for undergraduate degree-seeking veterans and their eligible dependents under the GI Bill entitlement.",
    city: 'Dallas',
    address: '6425 Boaz Lane, Dallas, TX 75205',
    resource_type: 'Education',
    assistance_type: 'Courses',
    application_required: 'No',
    email: 'vabenefits@smu.edu',
    phone: '214-768-4348',
    size: '7,056',
    statistics: '375',
    website: 'https://www.smu.edu/',
    yellowRibbon: 'Yes',
    src: 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3352.152347862848!2d-96.78938841793219!3d32.8412177056613!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x864e9f0447c04e15%3A0xc05fcab81b99542e!2sSouthern%20Methodist%20University!5e0!3m2!1sen!2sus!4v1707889780158!5m2!1sen!2sus',
    house: '/housing/',
    mental: '/healthcare/',
    video: 'https://www.youtube.com/embed/UmsDf1VeAfM?si=1P7LNrJhyAJ3Ceaa'
  }
];

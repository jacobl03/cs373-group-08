import houston from '../assets/housing_images/Tunnel_to_Towers_Houston.png';
import austin from '../assets/housing_images/Austin_Housing.png';
import dallas from '../assets/housing_images/Dallas_Housing.jpg';

export const card_data = [
  {
    // for card bar
    header: 'Caritas of Austin',
    data: 'Caritas of Austin is relentlessly committed to our vision of making homelessness rare, brief, and nonrecurring in Greater Austin. If you are experiencing homelessness, the first step to accessing services is completing a Coordinated Entry survey. This tool is used by numerous community organizations and is our city’s most effective way to prioritize the most vulnerable people for housing services. In 2021, the annual Homeless Point-In-Time Count identified 3,160 people experiencing homelessness in the Austin area, but it’s estimated that over 10,000 people access homeless services over the course of a year. Only a fraction are the “visible homeless” you may see on the street corners. The majority are people you don’t see or consider, from families living in their car to young people aging out of the foster care system. We know the most effective solutions to end homelessness in our community, but we can’t solve homelessness unless we do it together.',
    img: austin,
    city: 'Austin',
    // for instance page
    address: '4430 Menchaca Road or 505 Barton Springs Road',
    type_of_housing: 'Permanent and Temporary',
    governent_aided_initiative: 'Yes',
    family_housing_support: 'Yes',
    video: 'https://www.youtube.com/embed/a8IFwBNccL8?si=KMRyOEvj9R4ymKRf',
    mental: '/healthcare/',
    school: '/education/',
    src: 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3464.995832463158!2d-95.34481932467978!3d29.719879975088656!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8640be5a8ebea199%3A0xc9f387eca8247e7!2sUniversity%20of%20Houston!5e0!3m2!1sen!2sus!4v1707889731564!5m2!1sen!2sus'
  },
  {
    header: 'Tunnel to Towers Foundation',
    data: 'The Tunnel to Towers Foundation has completed a full-scale conversion of a former hotel in Houston, TX into a high-end, affordable apartment complex, which now provides permanent and transitional housing to 131 veterans in the greater Houston area. The property is equipped with a comprehensive service center on the first floor, offering veterans a litany of needed supportive services, provided by U.S. VETS. Phase II of our Houston Veterans Village will complement the apartment complex with 17 Comfort Homes, serving as permanent housing for senior citizen veterans. The Tunnel to Towers Foundation is committed to ending veteran homelessness nationwide. We honor the sacrifice and dignity of the American Service Member by ensuring that no veteran is left out on the streets of the country they volunteered to defend. ',
    img: houston,
    city: 'Houston',
    address: '18818 Tomball Parkway',
    type_of_housing: 'Permanent and transitional',
    governent_aided_initiative: 'No',
    family_housing_support: 'No',
    video: 'https://www.youtube.com/embed/1pjMbvU2UTc?si=8BHYKuNNwjracnJq',
    mental: '/healthcare/',
    src: 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3464.995832463158!2d-95.34481932467978!3d29.719879975088656!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8640be5a8ebea199%3A0xc9f387eca8247e7!2sUniversity%20of%20Houston!5e0!3m2!1sen!2sus!4v1707889731564!5m2!1sen!2sus',
    school: '/education/'
  },
  {
    header: 'City of Dallas - Office of Homeless Solutions',
    data: 'Our innovative solutions ensure our homeless neighbors receive food, shelter, housing, help to overcome barriers, and opportunities for a better life. We seek to provide public and private partners with the tools, resources, and opportunities to join the cause and help unsheltered individuals in our community. Funding for low barrier housing types, to include permanent supportive housing, targeting chronic homelessness; rapid rehousing addressing the elderly, disabled, families with children and young adults, ensuring that program participants are in compliance with the requirements of their housing applications; and Day Centers for seamless wrap-around services.',
    img: dallas,
    city: 'Dallas',
    address: '1500 Marilla St',
    type_of_housing: 'Permanent and Temporary',
    governent_aided_initiative: 'Yes',
    family_housing_support: 'Yes',
    video: 'https://www.youtube.com/embed/vt9dARgn9GQ?si=h854zDzMzZhu24qi',
    mental: '/healthcare/',
    src: 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3464.995832463158!2d-95.34481932467978!3d29.719879975088656!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8640be5a8ebea199%3A0xc9f387eca8247e7!2sUniversity%20of%20Houston!5e0!3m2!1sen!2sus!4v1707889731564!5m2!1sen!2sus',
    school: '/education/'
  }
];

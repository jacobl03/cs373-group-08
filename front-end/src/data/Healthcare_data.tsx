import houston from '../assets/healthcare_images/Houston_healthcare.png';
import austin from '../assets/healthcare_images/austin_healthcare.png';
import dallas from '../assets/healthcare_images/Dallas_healthcare.png';

export const healthcare_data = [
  {
    name: 'Austin Healthcare Facilities for Veterans',
    img: austin,
    description:
      "This is a VA location or in-network community care provider for same-day care for minor illnesses or injuries. Name of the facility: VA Regional Benefit Satellite Office at Austin VA Clinic. The multispecialty clinic offers primary care and specialty health services, including a chiropractic clinic, vision care (optometry), dentistry, oncology, treatment for traumatic brain injuries (TBI), foot care (podiatry), and more. ",
    city: 'Austin',
    address: '1700 N. Congress Avenue, Room 840A, Austin, TX 78701',
    contact_info: '800-252-8387',
    type_of_support: 'Mental Health',
    assistance_type: 'VA Benefits Facility',
    additional_fees: 'No',
    house: '/housing/',
    school: '/education/',
    src: 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3445.515257609595!2d-97.7391464!3d30.2793899!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8644b5ee9b7fd787%3A0xa20cdf18940d452c!2s1700%20N%20Congress%20Ave%20%23840a%2C%20Austin%2C%20TX%2078701!5e0!3m2!1sen!2sus!4v1707968503506!5m2!1sen!2sus',
    video: 'https://www.youtube.com/embed/IbEgr9rs2Zs'
  },
  {
    name: 'Houston Healthcare Facilities for Veterans',
    img: houston,
    description:
      "Houston's Healthcare Facilities for Veterans. This is a VA location or in-network community care provider for same-day care for minor illnesses or injuries. Name of the facility: Houston VA Regional Benefit Office. It offers counseling about eligibility for VA benefits and how to apply. In addition to that, it provides information about VA health care and memorial benefits.",
    city: 'Houston',
    address: '6900 Almeda Rd, Houston, TX 77030',
    contact_info: '281-447-8686',
    type_of_support: 'Physical Health',
    assistance_type: 'VA Benefits Facility',
    additional_fees: 'Yes',
    house: '/housing/',
    school: '/education/',
    src: 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3465.605959390244!2d-95.39039307445928!3d29.702201875097742!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8640bf8cc2842f69%3A0xeb55297edc011c80!2sVA%20Houston%20Regional%20Office!5e0!3m2!1sen!2sus!4v1707968709139!5m2!1sen!2sus',
    video: 'https://www.youtube.com/embed/vDmw3k_hnys'
  },
  {
    name: 'Dallas Vet Center',
    img: dallas,
    description:
      "Dallas's Healthcare Facilities for Veterans. This is a VA location or in-network community care provider for same-day care for minor illnesses or injuries. Name of the facility: Dallas Vet Center. They offer confidential help for Veterans, service members, and their families at no cost in a non-medical setting. Their services include counseling for needs such as depression, post traumatic stress disorder (PTSD), and the psychological effects of military sexual trauma (MST). They can also connect you with more support in VA and your community.",
    city: 'Dallas',
    address: '8610 Greenville Ave #125, Dallas, TX 75243',
    contact_info: '214-361-5896',
    type_of_support: 'Mental Health',
    assistance_type: 'Support Group',
    additional_fees: 'No',
    house: '/housing/',
    school: '/education/',
    src: 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3350.154582075595!2d-96.75530932373746!3d32.89408097799314!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x864e9fe0af89668b%3A0x33cefb74997add0!2sDallas%20Vet%20Center!5e0!3m2!1sen!2sus!4v1707968826594!5m2!1sen!2sus',
    video: 'https://www.youtube.com/embed/Z14YZhIQs4c'
  }
];

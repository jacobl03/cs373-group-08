import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';

import Home from './pages/Home';
import About from './pages/About';
import Housing from './pages/Housing';
import Education from './pages/Education';
import Healthcare from './pages/Healthcare';
import Navbar from './components/Navbar';
import HousingInstance from './pages/HousingInstance';
import EducationInstance from './pages/EducationInstance';
import HealthcareInstance from './pages/HealthcareInstance';

function App() {
  return (
    <div>
      <Navbar />
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="About" element={<About />} />
          <Route path="Housing" element={<Housing />} />
          <Route path="Education" element={<Education />} />
          <Route path="Healthcare" element={<Healthcare />} />
          <Route path="/housing/:index" element={<HousingInstance />} />
          <Route path="/education/:index" element={<EducationInstance />} />
          <Route path="/healthcare/:index" element={<HealthcareInstance />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;

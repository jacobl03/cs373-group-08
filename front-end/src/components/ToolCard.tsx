import React from 'react';

interface ToolCardProps {
  index: number;
  logo: string;
  name: string;
}

function ToolCard(props: ToolCardProps) {
  return (
    <div key={props.index} className="tool-card">
      <div className="card">
        <div style={{ padding: '1rem' }}>
          <img
            src={props.logo}
            className="card-img-top"
            alt={`${props.name} Logo`}
            style={{ height: '125px', objectFit: 'contain' }}
          />
        </div>
        <div className="card-body">
          <h5 className="text-center card-title">{props.name}</h5>
        </div>
      </div>
    </div>
  );
}

export default ToolCard;

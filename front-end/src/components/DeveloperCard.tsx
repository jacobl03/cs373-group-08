import React from 'react';
import '../styles/Card.css';

interface DeveloperCardProps {
  key: number;
  img_src: string;
  name: string;
  description: string;
  role: string;
  commits: number;
  issues: number;
  tests: number;
}

function DeveloperCard(props: DeveloperCardProps) {
  return (
    <div key={props.key} className="developer-card">
      <div className="card">
        <img
          src={props.img_src}
          className="card-img-top"
          alt={`${props.name}`}
          style={{ height: '400px', objectFit: 'cover' }}
        />
        <div className="card-body">
          <h5 className="card-title">{props.name}</h5>
          <p className="card-description">{props.description}</p>
          <p className="card-text">Role: {props.role}</p>
          <p className="card-stat">Number of Commits: {props.commits}</p>
          <p className="card-stat">Number of Issues: {props.issues}</p>
          <p className="card-stat">Number of Tests: {props.tests}</p>
        </div>
      </div>
    </div>
  );
}

export default DeveloperCard;

import * as React from 'react';
import axios from 'axios';
import '@mui/material';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import '../styles/Card.css';

interface EducationData {
  id: number;
  name: string;
  instance: number;
  img_src: string;
  description: string;
  city_id: number;
  contribution: string;
  degree: string;
  distance_learning_offered: string;
  funding: string;
  address: string;
  resource_type: string;
  link: string;
  related_housing_id: number;
  related_healthcare_id: number;
}

const baseURL = 'https://api.veteranhaven.me/education';
const cityBaseURL = 'https://api.veteranhaven.me/cities';

export default function EducationCard(props: EducationData) {
  const [response, setResponse] = React.useState<EducationData | null>(null);
  const [cityName, setCityName] = React.useState<string | null>(null);
  const [imgName, setImgName] = React.useState('');


  React.useEffect(() => {
    axios.get(baseURL + '/' + props.id).then((response) => {
      import(`../assets/education_instance_images/${response.data.img_src}`).then((img) => {
        setImgName(img.default);
        setResponse(response.data);
        }).catch((error) => {
          console.error('Error importing image:', error);
      });
      axios.get(cityBaseURL + "/" + response.data.city_id)
          .then(cityResponse => {
            setCityName(cityResponse.data.city_name);
          })
          .catch(cityError => {
            console.error('Error fetching city data:', cityError);
          });
    });
  }, [props.id, props.instance]);
  return (
    <div className="card__box">
      <Card sx={{ width: 400, height: 520, backgroundColor: '#7c826b', display: 'flex', flexDirection: 'column' }}>
        <CardMedia sx={{ height: 400 }} image={imgName} title="image example" />
        <CardContent sx={{flex: 1}}>
        {response && (
            <Typography
              gutterBottom
              variant="h4"
              component="div"
              color="#F1F0E2"
              fontFamily="Raleway"
              fontWeight="600">
              {response.name}
            </Typography>
          )}
          {response && (
            <div>
              <Typography variant="h6" color="#F1F0E2" fontWeight="light" fontFamily="Poppins">
              Services: {response.description ? response.description : "Visit website or look online for more information about services offered."}
              </Typography>
            </div>
          )}

          {response && (
            <Typography variant="h6" color="#F1F0E2" fontWeight="bold" fontFamily="Poppins">
              City: {cityName}
            </Typography>
          )}
          {response && (
            <Typography variant="h6" color="#F1F0E2" fontWeight="bold" fontFamily="Poppins">
              Degree Type: {response.degree}
            </Typography>
          )}
          {response && (
            <Typography variant="h6" color="#F1F0E2" fontWeight="bold" fontFamily="Poppins">
              Resource Type: Education
            </Typography>
          )}
          {response && (
            <Typography variant="h6" color="#F1F0E2" fontWeight="bold" fontFamily="Poppins">
              Website: {response.link ? response.link : "Website not provided."}
            </Typography>
          )}
           {response && (
            <Typography variant="h6" color="#F1F0E2" fontWeight="bold" fontFamily="Poppins">
              {isNaN(parseInt(response.funding)) ? `Funding Available For ${response.funding}` : `Funding Available for ${parseInt(response.funding)} students`}
            </Typography>
          )}
        </CardContent>
        <CardActions>
          <Button
            variant="outlined"
            size="small"
            sx={{ backgroundColor: '#F1F0E2', color: '#7c826b' }}
            href={`/education/${props.id}`}
          >
            Learn More
          </Button>
          <Button
            variant="outlined"
            size="small"
            sx={{ backgroundColor: '#EBD1AE', color: '#7c826b' }}
            href={`/housing/${1}`}>
            Related {cityName} Housing 
          </Button>
          <Button
            variant="outlined"
            size="small"
            sx={{ backgroundColor: '#EDE6C4', color: '#7c826b' }}
            href={`/healthcare/${response?.related_healthcare_id}`}>
            Related {cityName} Education 
          </Button>
        </CardActions>
      </Card>
    </div>
  );
}

import * as React from 'react';

import '@mui/material';
import axios from 'axios';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import '../styles/Card.css';
import '../styles/Text.css';

interface HousingData {
  name: string;
  id: number;
  instance: number;
  img_src: string;
  description: string;
  city_id: number;
  address: string;
  phone: string;
  hours: string;
  eligibility_requirements: string;
  areas_served: string;
  type_of_housing: string;
  website_link: string;
  related_healthcare_id: number;
  related_education_id: number;
}

const baseURL = 'https://api.veteranhaven.me/housing';
const cityBaseURL = 'https://api.veteranhaven.me/cities';

export default function HousingCard(props: HousingData) {
  const [response, setResponse] = React.useState<HousingData | null>(null);
  const [cityName, setCityName] = React.useState<string | null>(null);
  const [imgName, setImgName] = React.useState('');

  const extractDescription = (descriptionString: string): string[] => {
    const strippedString = descriptionString.replace(/["[\]]/g, ''); 
    return strippedString.split(',').map(item => item.trim()); 
  };

  React.useEffect(() => {
    axios.get(baseURL + '/' + props.id).then((response) => {
        import(`../assets/housing_instance_images/${response.data.img_src}`).then((img) => {
          setImgName(img.default);
          // Do whatever you want with the imported image 'img'
          const descriptionArray = extractDescription(response.data.description);
          const limitedDescription = descriptionArray.slice(0, 3).join(', ');
          const parsedResponse = { ...response.data, description: limitedDescription, img: img.default };
          setResponse(parsedResponse);
      }).catch((error) => {
          console.error('Error importing image:', error);
      });

      axios.get(cityBaseURL + "/" + response.data.city_id)
          .then(cityResponse => {
            setCityName(cityResponse.data.city_name);
          })
          .catch(cityError => {
            console.error('Error fetching city data:', cityError);
          });
    });
  }, [props.id, props.instance]);
  return (
    <div className="card__box">
      <Card sx={{ width: 400, height: 520, backgroundColor: '#7c826b', display: 'flex', flexDirection: 'column' }}>
        <CardMedia 
          sx={{ height: 300 }} 
          image={imgName} 
          title={response?.name} />
        <CardContent sx={{flex: 1}}>
          <Typography
            gutterBottom
            variant="h4"
            component="div"
            color="#F1F0E2"
            fontFamily="Raleway"
            fontWeight="600">
            {response?.name}
          </Typography>
          <Typography variant="h6" color="#F1F0E2" fontWeight="light" fontFamily="Poppins">
            {response?.description.slice(0, 100)}...
          </Typography>
          <Typography variant="h6" color="#F1F0E2" fontWeight="bold" fontFamily="Poppins">
            City: {cityName}
          </Typography>
          <Typography variant="h6" color="#F1F0E2" fontWeight="bold" fontFamily="Poppins">
            Type of Housing: Government Aided
          </Typography>
          <Typography variant="h6" color="#F1F0E2" fontWeight="bold" fontFamily="Poppins">
            Hours of Operation: {response?.hours ? response?.hours : "Not Available"}
          </Typography>
          <Typography variant="h6" color="#F1F0E2" fontWeight="bold" fontFamily="Poppins">
            Phone Number: {response?.phone}
          </Typography>
        </CardContent>
        <CardActions>
          <Button
            variant="outlined"
            size="small"
            sx={{ backgroundColor: '#F1F0E2', color: '#7c826b' }}
            href={`/housing/${props.id}`}>
            Learn More
          </Button>
          <Button
            variant="outlined"
            size="small"
            sx={{ backgroundColor: '#EBD1AE', color: '#7c826b'}}
            href={`/healthcare/${1}`}>
            Related {cityName} Healthcare 
          </Button>
          <Button
            variant="outlined"
            size="small"
            sx={{ backgroundColor: '#EDE6C4', color: '#7c826b' }}
            href={`/education/${response?.related_education_id}`}>
            Related {cityName} Education 
          </Button>
        </CardActions>
      </Card>
    </div>
  );
}

import * as React from 'react';
import axios from 'axios';
import '@mui/material';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import '../styles/Card.css';

interface instance {
  id: number;
  instance: number;
  name: string;
  img_src: string;
  description: string;
  city_id: number;
  address: string;
  phone: string;
  assistance_type: string;
  facility_type: string;
  vid_src: string;
  website: string;
  related_housing_id: number;
  related_education_id: number;
}

const baseURL = 'https://api.veteranhaven.me/healthcare';
const cityBaseURL = 'https://api.veteranhaven.me/cities';


export default function HealthcareCard(props: instance) {
  const [response, setResponse] = React.useState<instance | null>(null);
  const [cityName, setCityName] = React.useState<string | null>(null);
  const [imgName, setImgName] = React.useState('');

  const extractDescription = (descriptionString: string): string[] => {
    const strippedString = descriptionString.replace(/["[\]]/g, ''); 
    return strippedString.split(',').map(item => item.trim()); 
  };

  React.useEffect(() => {
    axios.get(baseURL + '/' + props.id).then((response) => {
        import(`../assets/health_instance_images/${response.data.img_src}`).then((img) => {
          setImgName(img.default);
          // Do whatever you want with the imported image 'img'
          const descriptionArray = extractDescription(response.data.description);
          const limitedDescription = descriptionArray.slice(0, 3).join(', ');
          const parsedResponse = { ...response.data, description: limitedDescription, img: img.default };
          setResponse(parsedResponse);
      }).catch((error) => {
          console.error('Error importing image:', error);
      });

      axios.get(cityBaseURL + "/" + response.data.city_id)
          .then(cityResponse => {
            setCityName(cityResponse.data.city_name);
          })
          .catch(cityError => { 
            console.log(response.data.city_id)
            console.error('Error fetching city data:', cityError);
          });
    });
  }, [props.id, props.instance]);

  return (
    <div className="card__box">
      <Card
        sx={{
          width: 400,
          height: 520,
          backgroundColor: '#7c826b',
          display: 'flex',
          flexDirection: 'column'
        }}>
        <CardMedia sx={{ height: 400 }} image={imgName} title="image example" />
        <CardContent sx={{ flex: 1 }}>
          {response && (
            <Typography
              gutterBottom
              variant="h4"
              component="div"
              color="#F1F0E2"
              fontFamily="Raleway"
              fontWeight="600">
              {response.name}
            </Typography>
          )}
          {response && (
            <div>
              <Typography variant="h6" color="#F1F0E2" fontWeight="light" fontFamily="Poppins">
              Services: {response.description ? response.description : "Visit website or look online for more information about services offered."}
              </Typography>
            </div>
          )}

          {response && (
            <Typography variant="h6" color="#F1F0E2" fontWeight="bold" fontFamily="Poppins">
              City: {cityName}
            </Typography>
          )}
          {response && (
            <Typography variant="h6" color="#F1F0E2" fontWeight="bold" fontFamily="Poppins">
              Type of Support: {response.assistance_type}
            </Typography>
          )}
          {response && (
            <Typography variant="h6" color="#F1F0E2" fontWeight="bold" fontFamily="Poppins">
              Facility Type: {response.facility_type}
            </Typography>
          )}
          {response && (
            <Typography variant="h6" color="#F1F0E2" fontWeight="bold" fontFamily="Poppins">
              Website: {response.website}
            </Typography>
          )}
        </CardContent>
        <CardActions>
          <Button
            variant="outlined"
            size="small"
            sx={{ backgroundColor: '#F1F0E2', color: '#7c826b' }}
            href={`/healthcare/${props.instance}`}>
            Learn More
          </Button>
          <Button
            variant="outlined"
            size="small"
            sx={{ backgroundColor: '#EBD1AE', color: '#7c826b'}}
            href={`/housing/${1}`}>
            Related {cityName} Housing 
          </Button>
          <Button
            variant="outlined"
            size="small"
            sx={{ backgroundColor: '#EDE6C4', color: '#7c826b' }}
            href={`/education/${response?.related_education_id}`}>
            Related {cityName} Education 
          </Button>
        </CardActions>
      </Card>
    </div>
  );
}

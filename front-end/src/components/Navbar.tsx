import React, { useState } from 'react';
import '../styles/Navbar.css';
import transparent from '../assets/transparent-logo.png';

function Navbar() {
  const [active, setActive] = useState('nav__menu');
  const [toggleIcon, setToggleIcon] = useState('nav__toggler');
  const navToggle = () => {
    active === 'nav__menu' ? setActive('nav__menu nav__active') : setActive('nav__menu');

    toggleIcon === 'nav__toggler'
      ? setToggleIcon('nav__toggler toggle')
      : setToggleIcon('nav__toggler');
  };
  return (
    <div>
      <nav className="nav">
      <a href="/" className="nav__logo">
          <img src={transparent}></img>
        </a>
        <ul className={active}>
          <li className="nav_menu">
            <a href="/" className="nav__link">
              {' '}
              Home{' '}
            </a>
          </li>
          <li className="nav_menu">
            <a href="/about" className="nav__link">
              {' '}
              About{' '}
            </a>
          </li>
          <li className="nav_menu">
            <a href="/housing" className="nav__link">
              {' '}
              Housing{' '}
            </a>
          </li>
          <li className="nav_menu">
            <a href="/education" className="nav__link">
              {' '}
              Education and Employment{' '}
            </a>
          </li>
          <li className="nav_menu">
            <a href="/healthcare" className="nav__link">
              {' '}
              Healthcare{' '}
            </a>
          </li>
        </ul>
        <div onClick={navToggle} className={toggleIcon}>
          <div className="line1"></div>
          <div className="line2"></div>
          <div className="line3"></div>
        </div>
      </nav>
    </div>
  );
}

export default Navbar;

import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { useParams, Link } from 'react-router-dom';
import Typography from '@mui/material/Typography';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import '../styles/InstancePage.css';
import MapComponent from '../components/MapComponent';

type paramType = {
  index: string;
};

interface HealthcareData {
  name: string;
  instance: number;
  img_src: string;
  description: string;
  city_id: number;
  address: string;
  phone: string;
  assistance_type: string;
  facility_type: string;
  vid_src: string;
  website: string;
  latitude: string;
  longitude: string;
  related_housing_id: number;
  related_education_id: number;
}

const baseURL = 'https://api.veteranhaven.me/healthcare';
const cityBaseURL = 'https://api.veteranhaven.me/cities';

export default function HealthcareInstance() {
  const { index } = useParams<paramType>();

  const [healthcareData, setHealthcareData] = useState<HealthcareData | null>(null);
  const [cityName, setCityName] = useState<string | null>(null);
  const [imgName, setImgName] = useState('');

  const extractDescription = (descriptionString: string): string[] => {
    const strippedString = descriptionString.replace(/["[\]]/g, '');
    return strippedString.split(',').map((item) => item.trim());
  };

  useEffect(() => {
    axios
      .get(baseURL + '/' + index)
      .then((response) => {
        import(`../assets/health_instance_images/${response.data.img_src}`).then((img) => {
          setImgName(img.default);
          // Do whatever you want with the imported image 'img'
          const descriptionArray = extractDescription(response.data.description);
          const limitedDescription = descriptionArray.slice(0, 3).join(', ');
          const parsedResponse = { ...response.data, description: limitedDescription, img: img.default };
          setHealthcareData(parsedResponse);
      }).catch((error) => {
          console.error('Error importing image:', error);
      });
        axios
          .get(cityBaseURL + '/' + response.data.city_id)
          .then((cityResponse) => {
            setCityName(cityResponse.data.city_name);
          })
          .catch((cityError) => {
            console.error('Error fetching city data:', cityError);
          });
      })
      .catch((error) => {
        console.error('Error fetching healthcare data:', error);
      });
  }, []);

  return (
    <div className="instance__page">
      <img className="instance__img" src={imgName}></img>
      <div className="instance__section">
        <div className="text__container">
          <Typography
            fontWeight="bold"
            paddingTop="1.5rem"
            gutterBottom
            variant="h2"
            component="div"
            color="#7C826B"
            fontFamily="Raleway">
            {healthcareData?.name}
          </Typography>
          <Typography variant="h4" color="#7C826B" textAlign="left" fontFamily="Poppins">
            Services:{' '}
            {healthcareData?.description
              ? healthcareData?.description
              : 'Visit website or look online for more information about services offered.'}
            <hr style={{ border: 0 }}></hr>
            
          </Typography>
        </div>
        <div className="card__container">
          <Card sx={{ width: 500, backgroundColor: '#7c826b' }}>
            <CardContent>
              <div style={{ width: '100%', height: '400px' }}>
                {healthcareData && (
                  <MapComponent latitude={healthcareData?.latitude} longitude={healthcareData?.longitude}/>
                )}
              </div>
              <Typography
                gutterBottom
                variant="h4"
                component="div"
                color="#F1F0E2"
                fontFamily="Raleway"
                fontWeight="600">
                Information
              </Typography>
              <Typography variant="h5" color="#F1F0E2" fontFamily="Poppins">
                City: {cityName}
              </Typography>
              <Typography variant="h5" color="#F1F0E2" fontFamily="Poppins">
                Address: {healthcareData?.address}
              </Typography>
              <Typography variant="h5" color="#F1F0E2" fontFamily="Poppins">
                Contact Info: {healthcareData?.phone}
              </Typography>
              <Typography variant="h5" color="#F1F0E2" fontFamily="Poppins">
                Type of Support: {healthcareData?.facility_type}
              </Typography>
              <Typography variant="h5" color="#F1F0E2" fontFamily="Poppins">
                Assistance Type: {healthcareData?.assistance_type}
              </Typography>
            </CardContent>
          </Card>
        </div>
      </div>
      <div className="footer">
        <Link to={`/housing/${healthcareData?.related_housing_id}`}>
          {' '}
          {cityName} Housing Resources{' '}
        </Link>{' '}
        |
        <Link to={`/education/${healthcareData?.related_education_id}`}>
          {' '}
          {cityName} University and Employment Support
        </Link>
      </div>
    </div>
  );
}

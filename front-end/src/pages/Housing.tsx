import React, { useState, useEffect } from 'react';
import HousingCard from '../components/HousingCard';
import axios from 'axios';
import Typography from '@mui/material/Typography';
import Pagination from '@mui/material/Pagination';
import Stack from '@mui/material/Stack';
import '../styles/Card.css';

interface HousingData {
  name: string;
  instance: number;
  img_src: string;
  description: string;
  city_id: number;
  address: string;
  phone: string;
  hours: string;
  eligibility_requirements: string;
  areas_served: string;
  type_of_housing: string;
  website_link: string;
  related_healthcare_id: number;
  related_education_id: number;
}

const baseURL = 'https://api.veteranhaven.me/housing';
const paginationURL = "https://api.veteranhaven.me/housing_page";

function Housing() {
  const [sortedData, setSortedData] = useState<HousingData[]>([]);
  const [pageData, setPageData] = useState<HousingData[]>([]);
  const [page, setPage] = React.useState(1);

  const handlePageChange = (event: React.ChangeEvent<unknown>, value: number) => {
    setPage(value);
    fetchPageData();
  };

  useEffect(() => {
    // setLoading(true);
    axios
      .get(baseURL)
      .then((response) => {
        const instances = response.data?.housing || [];
        setSortedData(instances);
        
      })
      .catch((error) => {
        console.error('Error fetching housing data:', error);
        
      });
  }, []);

  useEffect(() => {
    fetchPageData();
  }, [page]);

  const fetchPageData = () => {
    axios
      .get(`${paginationURL}?page=${page}`)
      .then((response) => {
        const instances = response.data?.housing || [];
        setPageData([]);
        setPageData(instances);
        // setLoading(false);
      })
      .catch((error) => {
        console.error('Error fetching housing data:', error);
        // setLoading(false);
      });
  };

  // const handleSortChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
  //   const sortBy = e.target.value;
  //   if (sortBy === 'ascending (A - Z)') {
  //     const sorted = [...sortedData].sort((a, b) => a.header.localeCompare(b.header));
  //     setSortedData(sorted);
  //   } else if (sortBy === 'descending (Z - A)') {
  //     const sorted = [...sortedData].sort((a, b) => b.header.localeCompare(a.header));
  //     setSortedData(sorted);
  //   } else if (sortBy == 'default') {
  //     setSortedData(card_data);
  //   }
  // };

  // const [selectedFilter, setSelectedFilter] = useState('');
  // const [selectedOption, setSelectedOption] = useState('');

  // const getUniqueOptions = (filter: string) => {
  //   const options: string[] = [];
  //   card_data.forEach((instance) => {
  //     const value = instance[filter as keyof HousingData];
  //     if (value && !options.includes(value)) {
  //       options.push(value);
  //     }
  //   });
  //   return options;
  // };

  // const filterOptions = selectedFilter ? getUniqueOptions(selectedFilter) : [];

  // const handleFilterChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
  //   setSelectedFilter(e.target.value);
  //   setSelectedOption('');
  //   setSortedData([...card_data]);
  // };

  // const handleOptionChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
  //   setSelectedOption(e.target.value);
  //   if (e.target.value !== '') {
  //     const filtered = card_data.filter(
  //       (instance) => instance[selectedFilter as keyof HousingData] === e.target.value
  //     );
  //     setSortedData(filtered);
  //   } else {
  //     setSortedData([...card_data]);
  //   }
  // };
  return (
    <div className="model__page">
      <div className="sort__container">
        <div className="sort__label">Total Instances: {sortedData.length}</div>
        {/* <label className="sort__label" htmlFor="sort">
          Sort by:
        </label>
        <select id="sort" onChange={handleSortChange}>
          <option value="default">Default</option>
          <option value="ascending (A - Z)">Ascending (A - Z)</option>
          <option value="descending (Z - A)">Descending (Z - A)</option>
        </select> */}
      </div>
      {/* <div className="filter__container">
        <label className="sort__label">Filter by:</label>
        <select value={selectedFilter} onChange={handleFilterChange}>
          <option value="">Select...</option>
          <option value="city">City</option>
          <option value="type_of_housing">Type of Housing</option>
          <option value="governent_aided_initiative">Government Aided Initiative</option>
          <option value="family_housing_support">Family Housing Support</option>
        </select>
        {selectedFilter && (
          <div>
            <label className="sort__label">Select {selectedFilter.replace('_', ' ')}:</label>
            <select value={selectedOption} onChange={handleOptionChange}>
              <option value="">All</option>
              {filterOptions.map((option) => (
                <option key={option} value={option}>
                  {option}
                </option>
              ))}
            </select>
          </div>
        )}
      </div> */}
      <div className="card__container">
        {pageData.map((instance, index) => (
          <div key={index}>
          <HousingCard key={index} id={instance.instance} {...instance} />
          </div>
        ))}
      </div>
      <div className="pagination__container">
      <Stack spacing={2}>
        <Typography variant="h4" fontFamily="Poppins"
        >Page: {page}</Typography>
        <Pagination
          count={Math.ceil(sortedData.length / 9)}
          page={page}
          onChange={handlePageChange}
          sx={{
            '& .MuiPaginationItem-root': {
              fontSize: '1.5rem', 
              minWidth: '30px', 
              minHeight: '30px', 
              margin: '0 5px', 
            },
            '& .MuiPaginationItem-ellipsis': {
              fontSize: '2rem', 
            },
          }}
        />
      </Stack>
      </div>
    </div>
  );
}

export default Housing;

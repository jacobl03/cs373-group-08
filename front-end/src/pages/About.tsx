import React, { useState, useEffect } from 'react';
import '../styles/About.css';
import '../styles/Text.css';
import axios from 'axios';
import DeveloperCard from '../components/DeveloperCard';
import ToolCard from '../components/ToolCard';
import { contributors, text, tools, data_sources } from '../data/About_data';

interface contributorProps {
  email: string;
  commits: number;
}

interface author {
  username: string;
}
interface issueProps {
  author: author;
}

function About() {
  const [contributorInfo, setContributorInfo] = useState([]);
  const [issueInfo, setIssueInfo] = useState([]);

  const projectId = 54622060;
  const gitLabToken = 'glpat-o2Cwd-51y9-p5uLDsDAf';

  useEffect(() => {
    // Fetch commit data
    axios
      .get(`https://gitlab.com/api/v4/projects/${projectId}/repository/contributors`, {
        headers: {
          'PRIVATE-TOKEN': gitLabToken
        }
      })
      .then((response) => {
        setContributorInfo(response.data);
        console.log(response.data);
      });

    // Fetch issue data
    axios
      .get(`https://gitlab.com/api/v4/projects/${projectId}/issues`, {
        headers: {
          'PRIVATE-TOKEN': gitLabToken
        }
      })
      .then((response) => {
        setIssueInfo(response.data);
        console.log(response.data);
      });
  }, [gitLabToken, projectId]);

  return (
    <div className="about__page">
      <div className="about__section">
        <h1 className="about__header">About Us</h1>
        <p className="about__text">{text[0].mission_statement}</p>
        <p className="about__text">{text[0].intended_users}</p>
        <p className="about__text">{text[0].disparate_data}</p>
      </div>

      <div className="about__section">
        <h1 className="about__header">Meet the Team!</h1>
        <div className="about__cards">
          <div className="row">
            {contributors.map((contributor, index) => {
              let numCommits = 0;
              contributor.email.forEach((email) => {
                // Filter contributors from API data matching the current email
                const contributors_filtered: contributorProps[] = contributorInfo.filter(
                  (contributor_from_api: contributorProps) => contributor_from_api.email.includes(email)
                );
                
                // Sum up the commits for each email address
                if (contributors_filtered.length > 0) {
                  contributors_filtered.forEach((filteredContributor) => {
                    numCommits += filteredContributor.commits;
                  });
                }
              });
              
              const numIssues = issueInfo.filter(
                (issue: issueProps) => issue.author.username === contributor.gitlab_username
              ).length;

              return (
                <DeveloperCard
                  key={index}
                  img_src={contributor.img_src}
                  name={contributor.name}
                  description={contributor.description}
                  role={contributor.role}
                  tests={contributor.tests}
                  commits={numCommits}
                  issues={numIssues}
                />
              );
            })}
          </div>
        </div>
        <h1 className="about__header">Totals</h1>
        <h4 className="about__header2">
          Total commits:{' '}
          {contributorInfo.reduce(
            (total, contributor: contributorProps) => total + contributor.commits,
            0
          )}
        </h4>
        <h4 className="about__header2">Total issues: {issueInfo.length}</h4>
      </div>
      <div className="about__section">
        <h1 className="about__header">Data Sources</h1>
        <div className="about__datasource">
          {data_sources.map((source) => (
            <h5 key={source.name}>
              <a style={{ color: '#ebd1ae', textDecoration: 'underline' }} href={source.link}>
                {source.name}
              </a>
              <p> {source.description} </p>
            </h5>
          ))}
        </div>
      </div>
      <div className="about__section">
        <h1 className="about__header">Tools Used</h1>
        <div className="about__cards">
          {tools.map((tool, index) => (
            <ToolCard key={index} {...tool} index={index} />
          ))}
        </div>
      </div>
    </div>
  );
}

export default About;

import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { useParams, Link } from 'react-router-dom';
import Typography from '@mui/material/Typography';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import '../styles/InstancePage.css';
import MapComponent from '../components/MapComponent';

type paramType = {
  index: string;
};

interface EducationData {
  id: number;
  name: string;
  instance: number;
  img_src: string;
  description: string;
  city_id: number;
  contribution: string;
  degree: string;
  distance_learning_offered: string;
  funding: string;
  address: string;
  resource_type: string;
  link: string;
  latitude: string;
  longitude: string;
  vid_src: string;
  related_housing_id: number;
  related_healthcare_id: number;
}

const baseURL = 'https://api.veteranhaven.me/education';
const cityBaseURL = 'https://api.veteranhaven.me/cities';

const defaultDescription = "This opportunity allows for veterans to commit to their education and opens up opportunities for their future career growth."

export default function EducationInstance() {
  const { index } = useParams<paramType>();

  const [educationData, setEducationData] = useState<EducationData | null>(null);
  const [cityName, setCityName] = useState<string | null>(null);
  const [imgName, setImgName] = React.useState('');

  useEffect(() => {
    axios
      .get(baseURL + '/' + index)
      .then((response) => {
        import(`../assets/education_instance_images/${response.data.img_src}`).then((img) => {
            setImgName(img.default);
            setEducationData(response.data);
            }).catch((error) => {
              console.error('Error importing image:', error);
          });
        axios
          .get(cityBaseURL + '/' + response.data.city_id)
          .then((cityResponse) => {
            setCityName(cityResponse.data.city_name);
          })
          .catch((cityError) => {
            console.error('Error fetching city data:', cityError);
          });
      })
      .catch((error) => {
        console.error('Error fetching education data:', error);
      });
  }, []);

  return (
    <div className="instance__page">
      <img className="instance__img" src={imgName}></img>
      <div className="instance__section">
        <div className="text__container">
          <Typography
            fontWeight="bold"
            paddingTop="1.5rem"
            gutterBottom
            variant="h2"
            component="div"
            color="#7C826B"
            fontFamily= 'Raleway'
          >
            {educationData?.name} 
           
          </Typography>
          <Typography variant="h4" color="#7C826B" textAlign="left" fontFamily= 'Poppins'>
            {defaultDescription}
            <hr style={{ border: 0 }}></hr>
          </Typography>
        </div>
        <div className="card__container">
        <Card sx={{ width: 500, backgroundColor: '#7c826b' }}>
            <CardContent>
              <div style={{ width: '100%', height: '400px' }}>
                {educationData && (
                  <MapComponent latitude={educationData?.latitude} longitude={educationData?.longitude}/>
                )}
              </div>
              <Typography gutterBottom variant="h4" component="div" color="#F1F0E2" fontFamily= 'Raleway' fontWeight='600'>
                Information
              </Typography>
              <Typography variant="h5" color="#F1F0E2" fontFamily= 'Poppins'>
                City Name: {cityName}
              </Typography>
              <Typography variant="h5" color="#F1F0E2" fontFamily= 'Poppins'>
                Address: {educationData?.address}
              </Typography>
              <Typography variant="h5" color="#F1F0E2" fontFamily= 'Poppins'>
                Degree Type: {educationData?.degree}
              </Typography>
              <Typography variant="h5" color="#F1F0E2" fontFamily= 'Poppins'>
                Resource Type: Education
              </Typography>
              <Typography variant="h5" color="#F1F0E2" fontFamily= 'Poppins'>
              Website: {educationData?.link !=="" ? educationData?.link : "Website not available."}
              </Typography>
              {educationData && (
                <Typography variant="h5" color="#F1F0E2" fontFamily= 'Poppins'>
                  {isNaN(parseInt(educationData.funding)) ? `Funding Available For ${educationData.funding}` : `Funding Available for ${parseInt(educationData.funding)} students`}
                </Typography>
              )}
            </CardContent>
          </Card>
        </div>
      </div>
      <div className="footer">
        <Link to={`/housing/${educationData?.related_housing_id}`}>
          {' '}
          {cityName} Housing Resources{' '}
        </Link>{' '}
        |
        <Link to={`/healthcare/${educationData?.related_healthcare_id}`}>
          {' '}
          {cityName} Health Care Resources
        </Link>
      </div>
    </div>
  );
}

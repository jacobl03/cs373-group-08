import React, { useState, useEffect } from 'react';
import axios from 'axios';
import EducationCard from '../components/EducationCard';
import '../styles/Card.css';
import Typography from '@mui/material/Typography';
import Pagination from '@mui/material/Pagination';
import Stack from '@mui/material/Stack';

interface EducationData {
  name: string;
  instance: number;
  img_src: string;
  description: string;
  city_id: number;
  contribution: string;
  degree: string;
  distance_learning_offered: string;
  funding: string;
  address: string;
  resource_type: string;
  link: string;
  related_housing_id: number;
  related_healthcare_id: number;
}
const baseURL = 'https://api.veteranhaven.me/education';
const paginationURL = "https://api.veteranhaven.me/education_page"

function Education() {
  const [sortedData, setSortedData] = useState<EducationData[]>([]);
  const [pageData, setPageData] = useState<EducationData[]>([]);
  const [page, setPage] = React.useState(1);

  const handlePageChange = (event: React.ChangeEvent<unknown>, value: number) => {
    setPage(value);
    fetchPageData();
  };

  useEffect(() => {
    // setLoading(true);
    axios
      .get(baseURL)
      .then((response) => {
        const instances = response.data?.education || [];
        setSortedData(instances);
        
      })
      .catch((error) => {
        console.error('Error fetching healthcare data:', error);
        
      });
  }, []);

  useEffect(() => {
    fetchPageData();
  }, [page]);

  const fetchPageData = () => {
    axios
      .get(`${paginationURL}?page=${page}`)
      .then((response) => {
        const instances = response.data?.education || [];
        setPageData([]);
        setPageData(instances);
        // setLoading(false);
      })
      .catch((error) => {
        console.error('Error fetching healthcare data:', error);
        // setLoading(false);
      });
  };
  // const handleSortChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
  //   const sortBy = e.target.value;
  //   if (sortBy === 'ascending (A - Z)') {
  //     const sorted = [...sortedData].sort((a, b) => a.name.localeCompare(b.name));
  //     setSortedData(sorted);
  //   } else if (sortBy === 'descending (Z - A)') {
  //     const sorted = [...sortedData].sort((a, b) => b.name.localeCompare(a.name));
  //     setSortedData(sorted);
  //   } else if (sortBy == 'default') {
  //     setSortedData(education_data);
  //   }
  // };

  // const [selectedFilter, setSelectedFilter] = useState('');
  // const [selectedOption, setSelectedOption] = useState('');

  // const getUniqueOptions = (filter: string) => {
  //   const options: string[] = [];
  //   education_data.forEach((instance) => {
  //     const value = instance[filter as keyof EducationData];
  //     if (value && !options.includes(value)) {
  //       options.push(value);
  //     }
  //   });
  //   return options;
  // };

  // const filterOptions = selectedFilter ? getUniqueOptions(selectedFilter) : [];

  // const handleFilterChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
  //   setSelectedFilter(e.target.value);
  //   setSelectedOption('');
  //   setSortedData([...education_data]);
  // };

  // const handleOptionChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
  //   setSelectedOption(e.target.value);
  //   if (e.target.value !== '') {
  //     const filtered = education_data.filter(
  //       (instance) => instance[selectedFilter as keyof EducationData] === e.target.value
  //     );
  //     setSortedData(filtered);
  //   } else {
  //     setSortedData([...education_data]);
  //   }
  // };
  return (
    <div className="model__page">
      <div className="sort__container"> Total Instances: {sortedData.length}</div>
        {/* <div className="sort__label">Total Instances: {education_data.length}</div>
        <label className="sort__label" htmlFor="sort">
          Sort by:
        </label>
        <select id="sort" onChange={handleSortChange}>
          <option value="default">Default</option>
          <option value="ascending (A - Z)">Ascending (A - Z)</option>
          <option value="descending (Z - A)">Descending (Z - A)</option>
        </select> */}
      {/* </div> */}
      {/* <div className="filter__container">
        <label className="sort__label">Filter by:</label>
        <select value={selectedFilter} onChange={handleFilterChange}>
          <option value="">Select...</option>
          <option value="city">City</option>
          <option value="resource_type">Resource Type</option>
          <option value="assistance_type">Assistance Type</option>
          <option value="application_required">Application Required</option>
        </select>
        {selectedFilter && (
          <div>
            <label className="sort__label">Select {selectedFilter.replace('_', ' ')}:</label>
            <select value={selectedOption} onChange={handleOptionChange}>
              <option value="">All</option>
              {filterOptions.map((option) => (
                <option key={option} value={option}>
                  {option}
                </option>
              ))}
            </select>
          </div>
        )}
      </div> */}
      <div className="card__container">
        {pageData.map((instance, index) => (
          <div key={index}>
          <EducationCard key={index} id={instance.instance} {...instance} />
          </div>
        ))}
      </div>
      <div className="pagination__container">
      <Stack spacing={2}>
        <Typography variant="h4" fontFamily="Poppins"
        >Page: {page}</Typography>
        <Pagination
          count={Math.ceil(sortedData.length / 9)}
          page={page}
          onChange={handlePageChange}
          sx={{
            '& .MuiPaginationItem-root': {
              fontSize: '1.5rem', 
              minWidth: '30px', 
              minHeight: '30px', 
              margin: '0 5px', 
            },
            '& .MuiPaginationItem-ellipsis': {
              fontSize: '2rem', 
            },
          }}
        />
      </Stack>
      </div>
    </div>
  );
}

export default Education;

import React, { useState, useEffect } from 'react';
import axios from 'axios';
import HealthcareCard from '../components/HealthcareCard';
import Typography from '@mui/material/Typography';
import Pagination from '@mui/material/Pagination';
import Stack from '@mui/material/Stack';
// import CircularProgress from '@mui/material/CircularProgress';
import '../styles/Card.css';

interface HealthcareData {
  name: string;
  instance: number;
  img_src: string;
  description: string;
  city_id: number;
  address: string;
  phone: string;
  assistance_type: string;
  facility_type: string;
  vid_src: string;
  website: string;
  related_housing_id: number;
  related_education_id: number;
}
const baseURL = 'https://api.veteranhaven.me/healthcare';
const paginationURL = "https://api.veteranhaven.me/healthcare_page";

function Healthcare() {
  const [sortedData, setSortedData] = useState<HealthcareData[]>([]);
  const [pageData, setPageData] = useState<HealthcareData[]>([]);
  const [page, setPage] = React.useState(1);
  // const [loading, setLoading] = useState<boolean>(true);

  const handlePageChange = (event: React.ChangeEvent<unknown>, value: number) => {
    setPage(value);
    fetchPageData();
  };

  useEffect(() => {
    // setLoading(true);
    axios
      .get(baseURL)
      .then((response) => {
        const instances = response.data?.health || [];
        setSortedData(instances);
        
      })
      .catch((error) => {
        console.error('Error fetching healthcare data:', error);
        
      });
  }, []);

  useEffect(() => {
    fetchPageData();
  }, [page]);

  const fetchPageData = () => {
    axios
      .get(`${paginationURL}?page=${page}`)
      .then((response) => {
        const instances = response.data?.health || [];
        setPageData([]);
        setPageData(instances);
        // setLoading(false);
      })
      .catch((error) => {
        console.error('Error fetching healthcare data:', error);
        // setLoading(false);
      });
  };

  // const handleSortChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
  //   const sortBy = e.target.value;
  //   if (sortBy === 'ascending (A - Z)') {
  //     const sorted = [...sortedData].sort((a, b) => a.name.localeCompare(b.name));
  //     setSortedData(sorted);
  //   } else if (sortBy === 'descending (Z - A)') {
  //     const sorted = [...sortedData].sort((a, b) => b.name.localeCompare(a.name));
  //     setSortedData(sorted);
  //   } else if (sortBy == 'default') {
  //     setSortedData(sortedData);
  //   }
  // };

  // const [selectedFilter, setSelectedFilter] = useState('');
  // const [selectedOption, setSelectedOption] = useState('');

  // const getUniqueOptions = (filter: string) => {
  //   const options: string[] = [];
  //   sortedData.forEach((instance) => {
  //     const value = instance[filter as keyof HealthcareData];
  //     if (value && !options.includes(value)) {
  //       options.push(value);
  //     }
  //   });
  //   return options;
  // };

  // const filterOptions = selectedFilter ? getUniqueOptions(selectedFilter) : [];

  // const handleFilterChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
  //   setSelectedFilter(e.target.value);
  //   setSelectedOption('');
  //   setSortedData([...sortedData]);
  // };

  // const handleOptionChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
  //   setSelectedOption(e.target.value);
  //   if (e.target.value !== '') {
  //     const filtered = healthcare_data.filter(
  //       (instance) => instance[selectedFilter as keyof HealthcareData] === e.target.value
  //     );
  //     setSortedData(filtered);
  //   } else {
  //     setSortedData([...healthcare_data]);
  //   }
  // };
  return (
    <div className="model__page">
      <div className="sort__label">Total Instances: {sortedData.length}</div>
      {/* {loading ? ( // Conditionally render loading circle
        <CircularProgress color="inherit" />
      ) : (<div></div>)} */}
      {/* <div className="sort__container">
        <div className="sort__label">Total Instances: {healthcare_data.length}</div>
        <label className="sort__label" htmlFor="sort">
          Sort by:
        </label>
        <select id="sort" >
          <option value="default">Default</option>
          <option value="ascending (A - Z)">Ascending (A - Z)</option>
          <option value="descending (Z - A)">Descending (Z - A)</option>
        </select>
      </div>
      <div className="filter__container">
        <label className="sort__label">Filter by:</label>
        <select value={selectedFilter} >
          <option value="">Select...</option>
          <option value="city">City</option>
          <option value="type_of_support">Type of Support</option>
          <option value="assistance_type">Assistance Type</option>
          <option value="additional_fees">Additional Fees</option>
        </select>
        {selectedFilter && (
          <div>
            <label className="sort__label">Select {selectedFilter.replace('_', ' ')}:</label>
            <select value={selectedOption} >
              <option value="">All</option>
              {filterOptions.map((option) => (
                <option key={option} value={option}>
                  {option}
                </option>
              ))}
            </select>
          </div>
        )}
      </div> */}
      <div className="card__container">
        {pageData.map((instance, index) => (
          <div key={index}>
          <HealthcareCard key={index} id={instance.instance} {...instance} />
          </div>
        ))}
      </div>
      <div className="pagination__container">
      <Stack spacing={2}>
        <Typography variant="h4" fontFamily="Poppins"
        >Page: {page}</Typography>
        <Pagination
          count={Math.ceil(sortedData.length / 9)}
          page={page}
          onChange={handlePageChange}
          sx={{
            '& .MuiPaginationItem-root': {
              fontSize: '1.5rem', 
              minWidth: '30px', 
              minHeight: '30px', 
              margin: '0 5px', 
            },
            '& .MuiPaginationItem-ellipsis': {
              fontSize: '2rem', 
            },
          }}
        />
      </Stack>
      </div>
    </div>
  );
}

export default Healthcare;

import React from 'react';
import { render, screen } from '@testing-library/react';
import Splash from '../src/pages/Home.tsx';
import About from '../src/pages/About.tsx';
import Education from '../src/pages/Education.tsx';
import Housing from '../src/pages/Housing.tsx';
import HealthCare from '../src/pages/Healthcare.tsx';
import NavBar from '../src/components/Navbar.tsx'
import App from '../src/App.tsx';

test('renders Veteran Haven splash page', () => {
	render(<Splash />);
	const welcomeTitle = screen.getByText('Welcome to Veteran Haven');
	expect(welcomeTitle).toBeInTheDocument();
});

test('renders About splash page', () => {
	render(<About />);
	const about = screen.getByText('about us');
	expect(about).toBeInTheDocument();
});

test('renders Education splash page', () => {
	render(<Education />);
	const edu = screen.getByText('University of Texas at Austin');
	expect(edu).toBeInTheDocument();
});

test('renders Housing splash page', () => {
	render(<Housing />);
	const house = screen.getByText('Caritas of Austin');
	expect(house).toBeInTheDocument();
});

test('renders HealthCare splash page', () => {
	render(<HealthCare />);
	const hc = screen.getByText('Austin Healthcare Facilities for Veterans');
	expect(hc).toBeInTheDocument();
});

test('renders NavBar', () => {
	render(<NavBar />);
	const bar = screen.getByText('About');
	expect(bar).toBeInTheDocument();
});

test('renders SUJITHA SEENIVASAN in about page', () => {
	render(<About />);
	const name = screen.getByText('SUJITHA SEENIVASAN');
	expect(name).toBeInTheDocument();
});

test('renders RAJVI SHAH in about page', () => {
	render(<About />);
	const name = screen.getByText('RAJVI SHAH');
	expect(name).toBeInTheDocument();
});

test('renders NATHAN HERNANDEZ in about page', () => {
	render(<About />);
	const name = screen.getByText('WELCOME TO VETERAN HAVEN');
	expect(name).toBeInTheDocument();
});

test('renders JACOB LOPEZ in about page', () => {
	render(<About />);
	const name = screen.getByText('WELCOME TO VETERAN HAVEN');
	expect(name).toBeInTheDocument();
});

test('renders CONNOR CHUNG in about page', () => {
	render(<About />);
	const name = screen.getByText('CONNOR CHUNG');
	expect(name).toBeInTheDocument();
});
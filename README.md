# cs373-group-08

**Canvas Group Number:** 8

**Names of the team members:** Rajvi Shah, Connor Chung, Sujitha Seenivasan, Jacob Lopez, Nathan Hernandez

**Name of the project:** Veteran Haven

**Website URL:** https://www.veteranhaven.me/

**Backend API URL:** https://api.veteranhaven.me/ 

**Postman Documentation:** https://documenter.getpostman.com/view/27079340/2sA2r3YRBy 

**Git SHA:** 32c7c0daa6862e3d46f861bbc2f02099b650543a

**Git SHA: fbc2bf77d914e9d128373db97757592c31b50780** 

**The proposed project:** We aim to create an informative, helpful resource to both educate the general public on major struggles in the veteran community and help Texas Veterans readjust to civilian life.


**URLs of at least three data sources that you will programmatically scrape:**
1. RESTful API: https://developer.va.gov/explore
2. https://api.va.gov/v0/gi/yellow_ribbon_programs
3. https://tx211tirn.communityos.org/guided_search_no_layout/render?ds=%7B%22service%5C%5Cservice_taxonomy%5C%5Cmodule_servicepost%22%3A%7B%22value%22%3A%5B%7B%22taxonomy_id%22%3A259%7D%5D%2C%22operator%22%3A%5B%22contains_array%22%5D%7D%2C%22agency%5C%5Cagency_system%5C%5Cname%22%3A%7B%22value%22%3A%22VLTEST%22%2C%22operator%22%3A%5B%22notequals%22%5D%7D%7D&localHistory=dYvulThlXlWcvTb0QZCC3g    

**Models:**
1. Education + Employment
2. Health Support   
3. Housing Resources

**An estimate of the number of instances of each model:** 10-20 instances per model

**Attributes of Each Model:**
1. Education + Employment
- Different institution offering further education for veterans - Name of the institution
- Address
- Contribution Amount
- Distance learning option available?
- Online option available?
- Degree Level
- Funding available for how many students? - Student Veteran link

2. Health Support
- Different clinics in that city - Name of the clinic
- Type
- Classification
- Website 
- Address 
- Phone

3. Housing Resources
- Housing facilities in that city - Name of that facility
- Address
- Phone
- Hours of operation
- Eligibility Requirements - Description
- Area Served
- Link


**describe five of those attributes for each model that you can filter or sort:**
1. Education + Employment
- Distance learning option available?
- Online option available?
- Degree Level
- Funding for how many people (you can sort this)
- Contribution Amount (you can also sort this)

2. Health Support
- Type
- Classification
- Website provided or not
- What services provided?
- Address available or not?

3. Housing Resources
- Address available or not?
- Hours of operation
- Eligibility Requirements - Description
- Area Served
- Link provided or not?



**instances of each model must connect to instances of at least two other models:**
1. Education + Employment
- Location (Health and Housing resources in the same area)
2. Health Support
- Location (Education and Housing resources in the same area)
3. Housing Resources
- Location (Education and Health resources in the same area)

**describe two types of media for instances of each model:**
1. Education + Employment
- Maps of Location
- Photos of the facility
2. Health Support
- Maps of Location
- Photos of the facility
3. Housing Resources
- Maps of Location
- Photos of the facility

**describe three questions that your site will answer:**
1. How can I support my local Texas veteran community?
2. Where can Texas veterans find support for readjustment after their service?
3. What are various resources out there for veterans to take advantage of?

**Estimated Time to Completion for Phase 1:** 
20 hours (avg)
**Actual time:**
Rajvi Shah: 17 hours
Connor Chung: 22 hours
Sujitha Seenivasan: 20 hours
Jacob Lopez: 20 hours
Nathan Hernandez: 20 hours

**Estimated Time to Completion for Phase 2:** 
20 hours (avg)
**Actual time:**
Rajvi Shah: 37 hours
Connor Chung: 35 hours
Sujitha Seenivasan: 45 hours
Jacob Lopez: 35 hours
Nathan Hernandez: 43 hours

Link to API Documentation on Postman: https://documenter.getpostman.com/view/27079340/2sA2r3YRBy 

**Phase 1: Leader**
Sujitha Seenivasan

**Phase 1: Leader Responsibilities**
- keeping track of goals and progress
- Delegating roles
- Sharing expertise with react and frontend

**Phase 2: Leader**
Nathan Hernandez

**Phase 2: Leader Responsibilities**
- keeping track of goals and progress
- Delegating roles
- Sharing expertise with SQL and backend

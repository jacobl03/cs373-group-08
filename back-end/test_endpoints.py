import unittest
from endpoints import app  

class TestEndpoints(unittest.TestCase):
    def setUp(self):
        app.config['TESTING'] = True
        self.app = app.test_client()

    def assertJsonResponse(self, response):
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content_type, 'application/json')
        self.assertTrue(response.is_json)
    
    def test_housing_page(self):
        response = self.app.get('/housing')
        self.assertEqual(response.status_code, 200)

    def test_housing_pagination(self):
        response = self.app.get('/housing_page?page=3')
        self.assertEqual(response.status_code, 200)
    
    def test_housing_instances(self):
        response = self.app.get('/housing/1')  
        self.assertEqual(response.status_code, 200)
    
    def test_education_page(self):
        response = self.app.get('/education')
        self.assertEqual(response.status_code, 200)
    
    def test_education_pagination(self):
        response = self.app.get('/education_page?page=9')
        self.assertEqual(response.status_code, 200)

    def test_education_instances(self):
        response = self.app.get('/education/3')
        self.assertEqual(response.status_code, 200)
    
    def test_health_page(self):
        response = self.app.get('/healthcare')
        self.assertEqual(response.status_code, 200)
    
    def test_health_pagination(self):
        response = self.app.get('/healthcare_page?page=5')
        self.assertEqual(response.status_code, 200)
    
    def test_health_instances(self):
        response = self.app.get('/healthcare/60')  
        self.assertEqual(response.status_code, 200)
    
    def test_housing_page_format(self):
        response = self.app.get('/housing')
        self.assertJsonResponse(response)

    def test_housing_pagination_format(self):
        response = self.app.get('/housing_page?page=3')
        self.assertJsonResponse(response)
    
    def test_housing_instances_format(self):
        response = self.app.get('/housing/1')  
        self.assertJsonResponse(response)
    
    def test_education_page_format(self):
        response = self.app.get('/education')
        self.assertJsonResponse(response)
    
    def test_education_pagination_format(self):
        response = self.app.get('/education_page?page=9')
        self.assertJsonResponse(response)

    def test_education_instances_format(self):
        response = self.app.get('/education/3')
        self.assertJsonResponse(response)
    
    def test_health_page_format(self):
        response = self.app.get('/healthcare')
        self.assertJsonResponse(response)
    
    def test_health_pagination_format(self):
        response = self.app.get('/healthcare_page?page=5')
        self.assertJsonResponse(response)
    
    def test_health_instances_format(self):
        response = self.app.get('/healthcare/60')  
        self.assertJsonResponse(response)

if __name__ == '__main__':
    unittest.main()

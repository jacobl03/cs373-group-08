import requests

def fetch_yellow_ribbon_programs(base_url, state, per_page=10):
    # Starting from the first page
    all_institutions = []

    for current_page in range (1, 20):
        # Constructing the URL with the current page and the desired state
        url = f"{base_url}?page={current_page}&per_page={per_page}&state={state}"
        
        # Make the HTTP GET request
        response = requests.get(url)

        if response.status_code == 200:
            data = response.json()
            institutions = data.get('data', [])
            all_institutions.extend(institutions)
        else:
            print(f"Failed to fetch data: {response.status_code}")
            break

    return all_institutions

# Base URL of the API
base_url = "https://api.va.gov/v0/gi/yellow_ribbon_programs"
# State for which we are fetching the programs
state = "TX"

# Fetch all Yellow Ribbon Programs for Texas
yellow_ribbon_programs = fetch_yellow_ribbon_programs(base_url, state)

# Processing the data (Example: Print the name and location of each institution)
for program in yellow_ribbon_programs:
    attributes = program['attributes']
    
    name = attributes['name_of_institution']
    city = attributes['city']
    degree_level = attributes['degree_level']
    address = f"{attributes['street_address']}, {city}, TX"
    contribution_amount = f"${float(attributes['contribution_amount']):,.2f}"
    distance_learning = "Yes" if attributes['distance_learning'] else "No"
    online_only = "Yes" if attributes['online_only'] else "No"
    student_veteran_link = attributes.get('student_veteran_link', 'Not provided')
    temp = attributes.get('number_of_students')
    number_students = "All eligible students" if temp == 99999 else temp
    degree_level = attributes.get('degree_level')
    
    print(f"Name of the institution: {name}")
    print(f"City: {city}")
    print(f"Address: {address}")
    print(f"Contribution Amount: {contribution_amount}")
    print(f"Distance Learning Available: {distance_learning}")
    print(f"Online option provided: {online_only}")
    print(f"Degree Level: {degree_level}")
    print(f"Funding available for how many students: {number_students}")
    print(f"Student veteran link: {student_veteran_link}")
    print(f"---------------------------------------------------")
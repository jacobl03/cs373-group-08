from geopy.geocoders import Nominatim
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine
from endpoints import Health, House, Education
from mapbox import Geocoder
from password import mapbox_api_key

DATABASE_URL = "mysql+pymysql://admin:Veteranhaven373@veteran-haven-db-1.cn6akukgs0pd.us-east-2.rds.amazonaws.com:3306/veteran_haven_db"
    

def update_health_coordinates():
    geolocator = Nominatim(user_agent="my_geocoder")

    mapbox_geocoder = Geocoder(access_token=mapbox_api_key)

    engine = create_engine(DATABASE_URL)
    Session = sessionmaker(bind=engine)
    session = Session()

    # Query all instances from the Health table
    health_instances = session.query(Health).filter((Health.latitude == None) | (Health.longitude == None)).all()

    for health_instance in health_instances:
        address = health_instance.address
        

        location = geolocator.geocode(address)

        if not location:
            response = mapbox_geocoder.forward(address)
            if response.status_code == 200 and response.json()['features']:
                result = response.json()['features'][0]
                coordinates = result['geometry']['coordinates']
                latitude, longitude = coordinates[1], coordinates[0]
                location = (latitude, longitude)
        

        if location:
            if isinstance(location, tuple):
                latitude, longitude = location
                health_instance.latitude = str(latitude)  
                health_instance.longitude = str(longitude)  
            else:
                health_instance.latitude = str(location.latitude)  
                health_instance.longitude = str(location.longitude)  
            session.commit()
            print(f"Updated coordinates for {address}")
        else:
            print(f"No coordinates found for {address}")

    session.close()

def update_housing_coordinates():
    geolocator = Nominatim(user_agent="my_geocoder")

    mapbox_geocoder = Geocoder(access_token=mapbox_api_key)

    engine = create_engine(DATABASE_URL)
    Session = sessionmaker(bind=engine)
    session = Session()

    # Query all instances from the House table
    house_instances = session.query(House).filter((House.latitude == None) | (House.longitude == None)).all()

    for house_instance in house_instances:
        address = house_instance.address

        #location = geolocator.geocode(address)
        location = None

        if not location:
            response = mapbox_geocoder.forward(address)
            if response.status_code == 200 and response.json()['features']:
                result = response.json()['features'][0]
                coordinates = result['geometry']['coordinates']
                latitude, longitude = coordinates[1], coordinates[0]
                location = (latitude, longitude)

        if location:
            if isinstance(location, tuple):
                latitude, longitude = location
                house_instance.latitude = str(latitude)  
                house_instance.longitude = str(longitude)  
            else:
                house_instance.latitude = str(location.latitude)  
                house_instance.longitude = str(location.longitude)  
            session.commit()
            print(f"Updated coordinates for {address}")
        else:
            print(f"No coordinates found for {address}")

    session.close()

def update_education_coordinates():
    geolocator = Nominatim(user_agent="my_geocoder")

    mapbox_geocoder = Geocoder(access_token=mapbox_api_key)

    engine = create_engine(DATABASE_URL)
    Session = sessionmaker(bind=engine)
    session = Session()

    # Query all instances from the Education table
    education_instances = session.query(Education).filter((Education.latitude == None) | (Education.longitude == None)).all()

    for education_instance in education_instances:
        address = education_instance.address

        location = geolocator.geocode(address)

        if not location:
            response = mapbox_geocoder.forward(address)
            if response.status_code == 200 and response.json()['features']:
                result = response.json()['features'][0]
                coordinates = result['geometry']['coordinates']
                latitude, longitude = coordinates[1], coordinates[0]
                location = (latitude, longitude)

        if location:
            if isinstance(location, tuple):
                latitude, longitude = location
                education_instance.latitude = str(latitude)  
                education_instance.longitude = str(longitude)  
            else:
                education_instance.latitude = str(location.latitude)  
                education_instance.longitude = str(location.longitude)  
            session.commit()
            print(f"Updated coordinates for {address}")
        else:
            print(f"No coordinates found for {address}")

    session.close()



def update_coords():
    update_health_coordinates()
    update_housing_coordinates()
    update_education_coordinates()

update_coords()

from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine
from endpoints import Health, House, Education, City, city_list


DATABASE_URL = "mysql+pymysql://admin:Veteranhaven373@veteran-haven-db-1.cn6akukgs0pd.us-east-2.rds.amazonaws.com:3306/veteran_haven_db"
    

def add_cities(queries):
    engine = create_engine(DATABASE_URL)
    Session = sessionmaker(bind=engine)
    session = Session()

    for key in queries:
        city = queries[key]
        
        existing_instance = session.query(City).filter(City.id == key).first()
        if existing_instance:
            continue
        
        else:
            new_city = City(city_name=city, id=key)

            # Add the new city to the session and commit
            session.add(new_city)
            session.commit()

    # Close the session
    session.close()
    
def add_gen():
    engine = create_engine(DATABASE_URL)
    Session = sessionmaker(bind=engine)
    session = Session()
    
    id = 0
    
    existing_instance = session.query(City).filter(City.id == id).first()
    if not existing_instance:
        new_city = City(city_name="General", id=id)
        session.add(new_city)
        session.commit()
    
    
add_gen()
add_cities(city_list)
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service as ChromeService
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import TimeoutException

# service = ChromeService()
# driver = webdriver.Chrome(service=service)

options = webdriver.ChromeOptions()
options.add_argument("--disable-dev-shm-usage")
driver = webdriver.Chrome(options=options)

# Goes to website
driver.get("https://tx211tirn.communityos.org/guided_search_no_layout/render?ds=%7B%22service%5C%5Cservice_taxonomy%5C%5Cmodule_servicepost%22%3A%7B%22value%22%3A%5B%7B%22taxonomy_id%22%3A259%7D%5D%2C%22operator%22%3A%5B%22contains_array%22%5D%7D%2C%22agency%5C%5Cagency_system%5C%5Cname%22%3A%7B%22value%22%3A%22VLTEST%22%2C%22operator%22%3A%5B%22notequals%22%5D%7D%7D&localHistory=kb_3XzsSUd5JZKQ3xHXXpg")

wait = WebDriverWait(driver, 40)

driver.implicitly_wait(40)

elems = []

# loops through every instance
for i in range(1, 201):
	driver.implicitly_wait(10)
	# gets instance, all the instances data except website
	cur = driver.find_element(By.XPATH, "/html/body/div[1]/div[2]/div[2]/div/div/div/div[4]/div/div/div[2]/div[3]/div/div/div[3]/div/div/div/div[2]/div[2]/div/div/div[1]/div[2]/div[" + str(i) + "]")
	driver.implicitly_wait(10)
	# gets instance website if it has it, some do not
	web = driver.find_element(By.XPATH, "/html/body/div[1]/div[2]/div[2]/div/div/div/div[4]/div/div/div[2]/div[3]/div/div/div[3]/div/div/div/div[2]/div[2]/div/div/div[1]/div[2]/div[" + str(i) + "]/div[1]/div[3]/div/div/div[2]/a")
	url = web.get_attribute('href')
	elems.append(cur.text + '\nLink: ' + url)

# Prints to housing.txt
# with open('housing.txt', 'w') as f:
#     # Redirecting print output to the file
#     for elem in elems:
#         print(elem + '\n', file=f)

cur_element = elems[0]
myDict = {
	"Antonio" : "San Antonio",
	"Paso": "El Paso",
	"Christi": "Corpus Christi",
	"Cavazos": "Fort Cavazos",
	"AFB": "Sheppard AFB",
	"Worth": "Fort Worth",
	"Station": "College Station",
	"Village": "Westworth Village",
	"Heights": "Harker Heights",
	"Spring": "Big Spring",
	"Stockton": "Fort Stockton",
	"Angelo": "San Angelo",
	"Prairie": "Grand Prairie",
	"Jackson": "Lake Jackson",
	"City": "Texas City",
	"Land": "Sugar Land",
	"Falls": "Wichita Falls",
	"Braunfels": "New Braunfels",
	"Park": "Cedar Park",
	"Cove": "Copperas Cove",
}
cityList = {""}

for elem in elems:
	line_num = 0
	lines = elem.split('\n')
	for line in lines:
		str1 = ""
		if line_num == 0 :
			str1 = "Name: " 
			print(str1,line)
		elif line_num == 1 :
			str1 = "City: "
			copy = line.split(' TX,')
			copy2 = copy[0].split(' ')
			lastWord = copy2[len(copy2) - 1]
			secondtoLastWord = copy2[len(copy2) - 2]
			if myDict.get(lastWord) is not None:
				city = secondtoLastWord + " " + lastWord
				cityList.add(city)
				print(str1,secondtoLastWord,lastWord)
			else:
				print(str1,lastWord)
				cityList.add(lastWord)
			str1 = "Address: "
			copy = line.split(', Located')[0]
			print(str1,copy)
		elif line_num == 2 :
			str1 = "Phone: "
			print(str1,line)
		elif line_num == 3 or line_num == 4 or line_num == 5:
			pass
		else: 
			print(line)
		line_num += 1
	print()
# print(elems[0] + '\n')


with open('./scraped_data/housing_cities.txt', 'w') as f:
    # Redirecting print output to the file
    for city in cityList:
        print(city, file=f)



driver.quit()
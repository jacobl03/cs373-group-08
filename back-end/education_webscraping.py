import os
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service as ChromeService
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException, TimeoutException, ElementClickInterceptedException

script_directory = os.path.dirname(os.path.abspath(__file__))
driver_path = os.path.join(script_directory, "chromedriver-mac-arm64/chromedriver")

service = ChromeService(executable_path=driver_path)
driver = webdriver.Chrome(service=service)

driver.get("https://www.va.gov/education/yellow-ribbon-participating-schools/?state=TX")

wait = WebDriverWait(driver, 10)

def scrape_page_data():
    
    # names
    wait.until(EC.presence_of_all_elements_located((By.XPATH, '//h3[@class="vads-u-margin--0"]')))
    names_facilities = [element.text for element in driver.find_elements(By.XPATH, '//h3[@class="vads-u-margin--0"]')]

    # cities
    location_elements_xpath = "//dt[contains(@class, 'sr-only') and contains(text(), 'School location')]/following-sibling::dd[1]"
    wait.until(EC.presence_of_all_elements_located((By.XPATH, location_elements_xpath)))
    location_elements = driver.find_elements(By.XPATH, location_elements_xpath)
    school_locations = [element.text.split(',')[0] for element in location_elements]

    #urls
    school_website_xpath = "//dt[contains(@class, 'vads-u-font-weight--bold') and contains(text(), 'School website')]/following-sibling::*[1]"
    school_website_elements = driver.find_elements(By.XPATH, school_website_xpath)

    urls = []
    for element in school_website_elements:
        if element.tag_name == "a":
            urls.append(element.get_attribute('href'))
        elif element.text == "Not provided":
            urls.append("Not provided")
        else:
            urls.append("https://" + element.text)

    #funding
    funding_elements_xpath = "//dt[contains(@class, 'vads-u-font-weight--bold') and contains(@class, 'vads-u-font-size--h5') and contains(text(), 'Maximum Yellow Ribbon funding amount')]/following-sibling::dd[1]"
    wait.until(EC.presence_of_all_elements_located((By.XPATH, funding_elements_xpath)))
    funding_elements = driver.find_elements(By.XPATH, funding_elements_xpath)
    funding_info = [element.text for element in funding_elements]

    #eligibility
    eligibility_elements_xpath = "//dt[contains(@class, 'vads-u-font-weight--bold') and contains(text(), 'Funding available for')]/following-sibling::dd[1]"
    wait.until(EC.presence_of_all_elements_located((By.XPATH, eligibility_elements_xpath)))
    eligibility_elements = driver.find_elements(By.XPATH, eligibility_elements_xpath)
    funding_eligibility = [element.text for element in eligibility_elements]

    #Degree_Type
    degree_type_xpath = "//dt[contains(@class, 'vads-u-font-weight--bold') and contains(text(), 'Degree type')]/following-sibling::dd[1]"
    wait.until(EC.presence_of_all_elements_located((By.XPATH, degree_type_xpath)))
    degree_type_elements = driver.find_elements(By.XPATH, degree_type_xpath)
    degree_types = [element.text for element in degree_type_elements]

    #School or program offered
    school_program_xpath = "//dt[contains(@class, 'school-program') and contains(text(), 'School or program')]/following-sibling::dd[1]"
    wait.until(EC.presence_of_all_elements_located((By.XPATH, school_program_xpath)))
    school_program_elements = driver.find_elements(By.XPATH, school_program_xpath)
    school_programs_offered = [element.text for element in school_program_elements]

    for name, location, funding, link, eligibility, degree_type, school in zip(names_facilities, school_locations, funding_info, urls, funding_eligibility, degree_types, school_programs_offered):
        print(f"Name: {name}, Location: {location}, Funding: {funding}, Link: {link}, Eligbility: {eligibility}, Degree Type: {degree_type}, What school or program offered: {school}")
        print("---------------------------------------------")

while True:

    scrape_page_data()

    try:
        next_button = wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="react-root"]/div/div/div[3]/va-pagination//ul[3]/li/button')))
        next_button.click()
        wait.until(EC.staleness_of(next_button))
    except (TimeoutException, NoSuchElementException):
        break 

driver.quit()


import re
import requests
import argparse

# Your API key
from password import api_key

# The API endpoint URL
url = 'https://sandbox-api.va.gov/services/va_facilities/v1/facilities'

# Headers including the API key
headers = {
    'apikey': api_key,
}

params = {
    'state': 'TX',
    'per_page': 10,
}

def add_spaces(service_name):
    return re.sub(r"(\w)([A-Z])", r"\1 \2", service_name)

def print_facility_info(facility):
    attributes = facility['attributes']
    print(f"Name: {attributes['name']}")
    print(f"City: {attributes['address']['physical']['city']}")
    print(f"Type: {attributes['facilityType']}")
    print(f"Classification: {attributes.get('classification', 'N/A')}")
    print(f"Website: {attributes.get('website', 'N/A')}")
    print(f"Address: {attributes['address']['physical']['address1']}, {attributes['address']['physical']['city']}, {attributes['address']['physical']['state']} {attributes['address']['physical']['zip']}")
    phone = attributes.get('phone', {}).get('main', 'N/A')
    print(f"Phone: {phone}")


def service_description(facility):
    attributes = facility['attributes']
    services = attributes.get('services', {})
    service_list = []
    for category in ['health', 'benefits', 'other']:
        category_services = services.get(category, [])
        for service in category_services:
            service_name = add_spaces(service['name'])
            service_list.append(service_name)
            
    val = ""
    for x in service_list:
        val = val +  x + " "
        
    print("Services: ", val)
    print("-------------------------------")

# Making a GET request to the API

parser = argparse.ArgumentParser()
args = parser.parse_args()


page = 1
total_pages = 1  # Start with 1 to enter the loop
while page <= total_pages:
    params['page'] = page
    response = requests.get(url, headers=headers, params=params)
    if response.status_code == 200:
        data = response.json()
        for facility in data['data']:
            print_facility_info(facility)
            service_description(facility)
        total_pages = data['meta']['pagination']['totalPages']
        page += 1
    else:
        print(f"Error fetching page {page}: {response.status_code}")
        break


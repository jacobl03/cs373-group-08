from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By

import time
import os


from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine
from endpoints import Health, House, Education


def set_zoom(driver, zoom_level):
    # Execute JavaScript to set zoom level
    driver.execute_script("document.body.style.zoom = '{}%'".format(zoom_level))

def is_file_in_directory(directory, filename):
    filepath = os.path.join(directory, filename)
    return os.path.exists(filepath) and os.path.isfile(filepath)

def get_images(queryList, folder_name):
    # What you enter here will be searched for in
    # Google Images
    options = webdriver.ChromeOptions()
    # MUST BE HEADLESS AND HAVE VERY LARGE WINDOW SIZE
    options.add_argument("--window-size=6000x5000")
    driver = webdriver.Chrome(options)
    # Maximize the screen
    driver.set_window_size(1920, 1080)
    # Open Google Images in the browser
    set_zoom(driver, 200)
    # Finding the search box

    if not os.path.exists(folder_name):
        os.makedirs(folder_name)

    for query in queryList :
        if is_file_in_directory(folder_name,f'{query}.png'):
            print("Continuing")
            continue
        driver.get('https://images.google.com/')
        box = driver.find_element(By.NAME, "q")
        # Type the search query in the search box
        box.send_keys(query)
        # Pressing enter
        box.send_keys(Keys.ENTER)
        time.sleep(5)
        # Locate each image using CSS_SELECTOR
        img_selector = f'#islrg div[data-ri="{0}"] img'
        images = driver.find_elements(By.CSS_SELECTOR, img_selector)
        # Ensure at least one image is found
        if images:
            img = images[0]
            # Enter the location of the folder in which
            # the images will be saved
            img.screenshot(f'{folder_name}/{query}.png')
            print("Saved screenshot for", query)
            # Just to avoid unwanted errors
            time.sleep(0.2)
    driver.close()

DATABASE_URL = "mysql+pymysql://admin:Veteranhaven373@veteran-haven-db-1.cn6akukgs0pd.us-east-2.rds.amazonaws.com:3306/veteran_haven_db"
 
def run_queries():
    engine = create_engine(DATABASE_URL)
    Session = sessionmaker(bind=engine)
    session = Session()

    # health_instances = session.query(Health).filter((Health.img_src == None)).all()
    # housing_instances = session.query(House).filter((House.img_src == None)).all()
    education_instances = session.query(Education).filter((Education.img_src == None)).all()

    queries = []
    # base_folder = '../front-end/src/assets/health_instance_images'
    # for health_instance in health_instances:
    #     queries.append(health_instance.name)

    base_folder = '../front-end/src/assets/education_instance_images'
    for education_instance in education_instances:
        queries.append(education_instance.name)
    
    # base_folder = '../front-end/src/assets/housing_images'
    # for housing_instance in housing_instances:
    #     queries.append(housing_instance.name)

    folder = base_folder
    get_images(queries, folder)

    # for health_instance in health_instances:
    #     health_instance.img_src = f'{health_instance.name}.png'
    for education_instance in education_instances:
        education_instance.img_src = f'{education_instance.name}.png'
    # for housing_instance in housing_instances:
    #     education_instance.img_src = f'{education_instance.name}.png'
    

    session.commit()



run_queries()
